//
//  Pokemon.swift
//  PokéAgenda
//
//  Created by Felipe Amaral on 4/9/16.
//  Copyright © 2016 Felipe Amaral. All rights reserved.
//

import Foundation
import Alamofire

class Pokemon {
    //Definindo nome e Id do pokemon
    private var _nome: String!
    private var _pokeagendaId: Int!
    private var _descricao: String!
    private var _tipo: String!
    private var _ataque: String!
    private var _peso: String!
    private var _defesa: String!
    private var _altura: String!
    private var _proximaEvolucaoTexto: String!
    private var _proximaEvolucaoId: String!
    private var _proximaEvolucaoLevel: String!
    private var _pokemonUrl: String!
    
    //Getters e Setters
    var nome: String
        {
            return _nome
    }
    
    var pokeagendaId: Int
        {
            return _pokeagendaId
    }
    
    var descricao: String
        {
            if _descricao == nil
            {
                _descricao = ""
            }
            return _descricao
    }
    
    var tipo: String
        {
            if _tipo == nil
            {
                _tipo = ""
            }
            return _tipo
    }
    
    var altura: String
        {
            if _altura == nil
            {
                _altura = ""
            }
            return _altura
    }
    
    var peso: String
        {
            if _peso == nil
            {
                _peso = ""
            }
            return _peso
    }
    
    var ataque: String
        {
            if _ataque == nil
            {
                _ataque = ""
            }
            return _ataque
    }
    
    var defesa: String
        {
            if _defesa == nil
            {
                _defesa = ""
            }
            return _defesa
    }
    
    var proximaEvolucaoTexto: String
        {
            if _proximaEvolucaoTexto == nil
            {
                _proximaEvolucaoTexto = ""
            }
            return _proximaEvolucaoTexto
    }
    
    var proximaEvolucaoId: String
        {
            if _proximaEvolucaoId == nil
            {
                _proximaEvolucaoId = ""
            }
            return _proximaEvolucaoId
    }
    
    //Initializer para facilitar a criação de cada pokemon
    init(nome: String, pokeagendaId: Int) {
        self._nome = nome
        self._pokeagendaId = pokeagendaId
        
        _pokemonUrl = "\(URL_BASE)\(URL_POKEMON)\(self._pokeagendaId)/"
    }

    
    func baixarDetalhesPokemon(completed: DownloadFinalizado)
    {
        let url = NSURL(string: _pokemonUrl)!
        Alamofire.request(.GET, url).responseJSON
            { (response: Response<AnyObject, NSError>) -> Void in
                if let dict = response.result.value as? Dictionary<String, AnyObject>
                {
                    if let peso = dict["weight"] as? String
                    {
                        self._peso = peso;
                    }
                    if let altura = dict["height"] as? String
                    {
                        self._altura = altura;
                    }
                    if let ataque = dict["attack"] as? Int
                    {
                        self._ataque = "\(ataque)";
                    }
                    if let defesa = dict["defense"] as? Int
                    {
                        self._defesa = "\(defesa)";
                    }
                    
                    if let tipos = dict["types"] as? [Dictionary<String, String>] where tipos.count > 0
                    {
                        if let nome = tipos[0]["name"]
                        {
                            self._tipo = nome.capitalizedString
                        }
                        if tipos.count > 1
                        {
                            for var x = 1; x < tipos.count; x++
                            {
                                if let nome = tipos[x]["name"]
                                {
                                    self._tipo! += "/\(nome.capitalizedString)"
                                }
                            }
                        }
                    }
                    else
                    {
                        self._tipo = "";
                    }
                    
                    if let descArr = dict["descriptions"] as? [Dictionary<String, String>] where descArr.count > 0
                    {
                        if let url = descArr[0]["resource_uri"]
                        {
                            let nsurl = NSURL(string: "\(URL_BASE)\(url)")!
                            Alamofire.request(.GET, nsurl).responseJSON
                                { (response: Response<AnyObject, NSError>) -> Void in
                                    
                                    if let descDict = response.result.value as? Dictionary<String, AnyObject>
                                    {
                                        if let descricao = descDict["description"] as? String
                                        {
                                            self._descricao = descricao
                                        }
                                    }
                                    
                                    completed()
                            }
                        }
                    }
                    else
                    {
                        self._descricao = ""
                    }
                    
                    if let evolucoes = dict["evolutions"] as? [Dictionary<String, AnyObject>] where evolucoes.count > 0
                    {
                        if let para = evolucoes[0]["to"] as? String
                        {
                            //Can't support Mega Pokemon right now
                            if para.rangeOfString("mega") == nil
                            {
                                if let uri = evolucoes[0]["resource_uri"] as? String
                                {
                                    let newStr = uri.stringByReplacingOccurrencesOfString("/api/v1/pokemon/", withString: "")
                                    let num = newStr.stringByReplacingOccurrencesOfString("/", withString: "")
                                    
                                    self._proximaEvolucaoId = num
                                    self._proximaEvolucaoTexto = para
                                    
                                    if let lvl = evolucoes[0]["level"] as? Int
                                    {
                                        self._proximaEvolucaoLevel = "\(lvl)"
                                    }
                                    else
                                    {
                                        if let method = evolucoes[0]["method"] as? String
                                        {
                                            self._proximaEvolucaoLevel = method
                                        }
                                    }
                                    
                                }
                            }
                        }
                    }
                    
                }
                
        }
    }
}