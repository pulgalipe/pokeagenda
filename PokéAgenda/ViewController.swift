//
//  ViewController.swift
//  PokéAgenda
//
//  Created by Felipe Amaral on 4/9/16.
//  Copyright © 2016 Felipe Amaral. All rights reserved.
//

import UIKit
//Definicao dos Delegates para que a CollectionView funcione
class ViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UISearchBarDelegate {
    
    @IBOutlet weak var collection: UICollectionView!
    
    @IBOutlet weak var barraDeBusca: UISearchBar!
    
    var pokemon = [Pokemon]()
    var resultadoBusca = [Pokemon]()
    var estaBuscando = false
    

    override func viewDidLoad() {
        super.viewDidLoad()
        var preferStatusBarHidden: Bool {
            return true
        }
        collection.delegate = self
        collection.dataSource = self
        barraDeBusca.delegate = self
        barraDeBusca.returnKeyType = UIReturnKeyType.Done
        
        parseInfoPokemonCSV()
    
    }
    //Funcao para parsear informacoes do CSV
    func parseInfoPokemonCSV() {
        //Definindo caminho do CSV e garantindo que ele vai estar naquele caminho
        let path = NSBundle.mainBundle().pathForResource("pokemons", ofType: "csv")!
        //Tentando ler os dados do arquivo CSV
        do {
            let csv = try CSV(contentsOfURL: path)
            let rows = csv.rows
            
            for row in rows {
                let pokemonId = Int(row["id"]!)!
                let nome = row["identifier"]!
                let poke = Pokemon(nome: nome, pokeagendaId: pokemonId)
                pokemon.append(poke)
            }
            
        } catch let err as NSError {
            print(err.debugDescription)
        }
    }
    
    //Delegate para definir qual item mostrar em cada index da celula
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        //Verifica se celula existe a reusa para economizar recursos na app
        if let cell = collectionView.dequeueReusableCellWithReuseIdentifier("PokeCell", forIndexPath: indexPath) as? PokeCell {
            
            let poke: Pokemon!
            
            //Verifica se esta buscando algo
            //Se esta buscando, retorna resultado da busca
            if estaBuscando {
                poke = resultadoBusca[indexPath.row]
            } else {
                //Senão, mostra todos pokemons
                poke = pokemon[indexPath.row]
            }
            
            //Apicando estilos a CollectionViewCell
            cell.configurarCell(poke)
            
            return cell
        }
        //Caso nao encontre retorne uma celula generica
        else {
            return UICollectionViewCell()
        }
    }
    
    //Delegate para carregar o conteudo na celula clicada
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        
        let poke: Pokemon!
        
        if estaBuscando {
            poke = resultadoBusca[indexPath.row]
        } else {
            poke = pokemon[indexPath.row]
        }
//        print(poke.nome)
        //Após clicar no pokemon, carregue XIB com o identificador
        performSegueWithIdentifier("DetalhePokemonVC", sender: poke)
        
    }
    
    //Delegate para mostrar numero de itens na sessao da Cell
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        //Se usuario esta buscando retorne o numero de pokemons na busca ao inves de todos pokemons na lista
        if estaBuscando {
            return resultadoBusca.count
        }
        //Senao retorna lista completa
        return pokemon.count
    }
    
    //Retorna o numero de sessoes da CollectionView
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    //Delegate para definir o tamanho do item no index da CollectionView
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        return CGSizeMake(105,105)
    }
    
    //Ao cicar no botão "Buscar" esconda o teclado
    func searchBarSearchButtonClicked(searchBar: UISearchBar) {
        view.endEditing(true)
    }
    
    //Metodo que busca toda vez que o texto da barra de busca for alterado
    func searchBar(searchBar: UISearchBar, textDidChange searchText: String) {
        //Verifica se o campo de busca é nil ou vazio
        if barraDeBusca.text == nil || barraDeBusca.text == "" {
            estaBuscando = false
            view.endEditing(true)
            collection.reloadData()
        }
        else {
            //Se for verdadeiro aplica um filtro no array de pokemons e retorna o resultado da busca
            estaBuscando = true
            let busca = barraDeBusca.text!.lowercaseString
            resultadoBusca = pokemon.filter({$0.nome.rangeOfString(busca) != nil})
            collection.reloadData()
        }
    }
    
    //Verifica em qual ViewController está para saber qual view mostrar
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "DetalhePokemonVC" {
            if let vcDetalhe = segue.destinationViewController as? DetalhePokemonVC {
                if let poke = sender as? Pokemon {
                    vcDetalhe.pokemon = poke
                }
            }
        }
    }
}

