//
//  PokeCell.swift
//  PokéAgenda
//
//  Created by Felipe Amaral on 4/9/16.
//  Copyright © 2016 Felipe Amaral. All rights reserved.
//

import UIKit

class PokeCell: UICollectionViewCell {
    //Define Outlets para adicionar aos elementos da CollectionView
    @IBOutlet weak var miniaturaImg: UIImageView!
    @IBOutlet weak var nomeLbl: UILabel!
    
    var pokemon: Pokemon!
    
    //Dando override no método init para alterar nosso frame e deixar a app mais 'bunitcha'
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        layer.cornerRadius = 5.0
    }
    
    //Metodo para atribuir nome e imagem aos elementos da CollectionViewCell
    func configurarCell (pokemon: Pokemon) {
        self.pokemon = pokemon
        nomeLbl.text = self.pokemon.nome.capitalizedString
        miniaturaImg.image = UIImage(named: "\(self.pokemon.pokeagendaId)")
    }
}
