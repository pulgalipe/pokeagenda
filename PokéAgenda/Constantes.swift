//
//  Constantes.swift
//  PokéAgenda
//
//  Created by Felipe Amaral on 4/15/16.
//  Copyright © 2016 Felipe Amaral. All rights reserved.
//

import Foundation

let URL_BASE = "http://pokeapi.co"
let URL_POKEMON = "/api/v1/pokemon/"

typealias DownloadFinalizado = () -> ()