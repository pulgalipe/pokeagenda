//
//  DetalhePokemonVC.swift
//  PokéAgenda
//
//  Created by Felipe Amaral on 4/10/16.
//  Copyright © 2016 Felipe Amaral. All rights reserved.
//

import UIKit

class DetalhePokemonVC: UIViewController {
    
    @IBOutlet weak var proximaEvolucaoLabel: UILabel!
    @IBOutlet weak var nomeLabel: UILabel!
    @IBOutlet weak var imagemPrincipal: UIImageView!
    
    @IBOutlet weak var terceiraEvolucao: UIImageView!
    @IBOutlet weak var segundaEvolucao: UIImageView!
    @IBOutlet weak var primeiraEvolucao: UIImageView!
    @IBOutlet weak var pokeIDLabel: UILabel!
    
    @IBOutlet weak var defesaLabel: UILabel!
    
    @IBOutlet weak var alturaLabel: UILabel!
    @IBOutlet weak var pesoLabel: UILabel!
    @IBOutlet weak var ataqueLabel: UILabel!
    @IBOutlet weak var tipoLabel: UILabel!
    @IBOutlet weak var descricaoLabel: UILabel!
    var pokemon: Pokemon!

    override func viewDidLoad() {
        super.viewDidLoad()
        let img = UIImage(named: "\(pokemon.pokeagendaId)")
        nomeLabel.text = pokemon.nome.capitalizedString
        imagemPrincipal.image = img
        primeiraEvolucao.hidden = true
        pokeIDLabel.text = "\(pokemon.pokeagendaId)"
        
        pokemon.baixarDetalhesPokemon
            { () -> () in
                //Esse metódo vai ser chamado após finalizar o download dos dados
                self.atualizarDados()
        }
    }
    
    func atualizarDados()
    {
        descricaoLabel.text = pokemon.descricao
        tipoLabel.text = pokemon.tipo
        alturaLabel.text = pokemon.altura
        ataqueLabel.text = pokemon.ataque
        pesoLabel.text = pokemon.peso
        defesaLabel.text = pokemon.defesa
        
        if pokemon.proximaEvolucaoTexto == ""
        {
            proximaEvolucaoLabel.text = "Sem evolução"
            segundaEvolucao.hidden = true
        }
        else
        {
            primeiraEvolucao.image = UIImage(named: "\(pokemon.proximaEvolucaoId)")
            primeiraEvolucao.hidden = false
            proximaEvolucaoLabel.text = "Próxima Evolução: \(pokemon.proximaEvolucaoTexto)"
        }
    }
    
    @IBAction func apertarBotaoParaVoltar(sender: UIButton) {
        dismissViewControllerAnimated(true, completion: nil)
    }

}
